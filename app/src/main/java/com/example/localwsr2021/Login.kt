package com.example.localwsr2021


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.view.View
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_login.*

class Login : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        button3.setOnClickListener {
            val log = email.text.toString()
            val pass = password.text.toString()
            if (log != "" && pass != "") {
                if (android.util.Patterns.EMAIL_ADDRESS.matcher(log).matches()) {
                    startActivity(Intent(this, Main::class.java))
                    finish()
                } else {
                    alert("Error", "Email is wrong")
                }
            }
        }
    }
    fun alert(title: String, mes: String) {
        AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(mes)
            .setNegativeButton("Close", null)
            .show()
    }

    fun onclick(v: View) {
        startActivity(Intent(this, Register::class.java))
    }
}
