package com.example.localwsr2021

import android.content.Intent
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.splashscreen)
        Handler().postDelayed({startActivity(Intent(this, OnBoarding::class.java) )},1500)
    }
}