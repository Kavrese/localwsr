package com.example.localwsr2021


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.localwsr2021.R
import android.content.Intent
import android.view.View
import kotlinx.android.synthetic.main.activity_profile.*

class Profile : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

    }

    fun onclick(v: View) {
        startActivity(Intent(this, Main::class.java))

    }
}