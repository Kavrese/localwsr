package com.example.localwsr2021


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_on_boarding.*

class OnBoarding : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_boarding)
        button.setOnClickListener() {startActivity(Intent(this, Login::class.java))  }
        textView3.setOnClickListener() {startActivity(Intent(this, Register::class.java))  }
    }
}